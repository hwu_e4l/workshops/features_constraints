#+TITLE: Stuff
#+AUTHOR: Vishakh Pradeep Kumar




Fire Suppression System

Need a way to ensure that power is cut off automatically and that there is a CO2 or similar canister to actually extinguish any open flames.

* Etchent Solution

Needs a decent starter
- Needs to have some kind of stirring mechanism
- Needs to have some kind of aquarium bubble tihngy to replenish with O2
- Something to avoid too many fumes.


* photoresist

photoresist is light-sensitive material that's applied to the surface of the pcb. When a positive photoresist is exposed to light, it degrades and can be removed. The exposed region can then be treated with some etchent to give you a bare pcb.

Light on photoresist | No copper
No light | Traces

In contrast, a negative photoresist hardens with exposure to light.

12 mil minimum trace width and 12 mm trace distance

So you could use translucent paper and an printer to generate your mask OR you could use a UV laser to just do it quickly


* Developing the board

In this step, the exposed photoresist is removed chemically using developer which leaves a positive of your layout on copper.

When you pour developer on the pcb with photoresist, the resist will be dissolved away.

Agitate the board with your hand and ensure that there is flow around the board.

Too long and the smaller traces disappear.
Too little and the etching process doesn't work.


* Carbide Drill bits of pretty small size

Need to drill holes and vias

* Reflow soldering

Will need a bunch of IR leds to uniformly heat up a small enclosed space.

* Shopping List

|---------------------------------+--------+-----------------|
| Item                            |  Price | Link            |
|---------------------------------+--------+-----------------|
| UV Protection Glasses           | 151.49 | [[https://www.amazon.ae/dp/B00UJ7R6GK/ref=asc_df_B00UJ7R6GK/?tag=googleshopp09-21&linkCode=df0&hvadid=382301900624&hvpos=1o8&hvnetw=g&hvrand=9135613043128853096&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=1000013&hvtargid=pla-492980426323&psc=1][Amazon Link]]     |
| Power Supply 60 W               |    100 | Naif            |
| Emergency Switch                |  12.97 | [[https://www.banggood.com/660V-10A-NO-NC-Push-Button-Momentary-Press-Emergency-Stop-Switch-p-918893.html?rmmds=buy&ID=228&cur_warehouse=CN][Banggood Link]]   |
| 405 nm 2.5W UV laser            | 200.54 | [[https://www.aliexpress.com/item/32955656582.html?spm=a2g0o.productlist.0.0.63a02713D0hMwe&algo_pvid=fdd4f800-52d4-4883-9873-f4a1aa14cbb7&algo_expid=fdd4f800-52d4-4883-9873-f4a1aa14cbb7-8&btsid=35139ca0-ef7c-4844-8833-d978a41adfc6&ws_ab_test=searchweb0_0,searchweb201602_3,searchweb201603_55][AliExpress Link]] |
| Polygon motor                   |    100 | nil             |
| Muriatic Acid                   |      0 |                 |
| Hydrogen Peroxide               |      0 |                 |
| Photoresist                     |     10 | [[https://www.banggood.com/30CM-1M-Portable-Photosensitive-Dry-Film-For-Circuit-Photoresist-Sheet-For-Plating-Hole-Covering-Etching-For-Producing-PCB-Board-p-1448473.html?rmmds=search&cur_warehouse=CN][Banggood Link]]   |
| Soldermask                      |     12 | [[https://www.banggood.com/MECHANIC-UV-Curable-Solder-Mask-10CC-for-PCB-BGA-Circuit-Board-Soldering-Welding-Paste-Flux-Oil-p-1299849.html?gmcCountry=AE&currency=AED&cur_warehouse=CN&createTmp=1&ID=231&utm_source=googleshopping&utm_medium=cpc_bgs&utm_content=frank&utm_campaign=ssc-ae-all-a06&ad_id=376006095084&gclid=CjwKCAiAx_DwBRAfEiwA3vwZYpFdKhThKqT2z9yiw45zDc9o6oGH3YvJ4Eskjx1mIo4KDIRUSNIebhoCWxIQAvD_BwE][Banggood Link]]   |
| Acetone                         |      0 |                 |
| PTFE Fittings and Tube          |  39.22 | [[https://www.amazon.ae/dp/B07JDHK47B/ref=asc_df_B07JDHK47B/?tag=googleshopp09-21&linkCode=df0&hvadid=390404115595&hvpos=1o3&hvnetw=g&hvrand=3797341991117626981&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=1000013&hvtargid=pla-616045008884&psc=1][Amazon Link]]     |
| 5pcs Nema 17 Stepper Motor 5pcs |    117 | [[https://www.amazon.ae/dp/B07W4HLN7B/ref=asc_df_B07W4HLN7B/?tag=googleshopp09-21&linkCode=df0&hvadid=390520753196&hvpos=1o5&hvnetw=g&hvrand=5654577159300063022&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=1000013&hvtargid=pla-829982568193&psc=1][Amazon]]          |
| Ball Bearing 608rs (20 pcs)     |  20.40 | [[https://www.banggood.com/20pcs-608rs-ABEC-9-Ball-Bearing-Carbon-Steel-Skateboard-Wheel-Bearings-p-1395046.html?gmcCountry=AE&currency=AED&createTmp=1&utm_source=googleshopping&utm_medium=cpc_bgs&utm_content=frank&utm_campaign=ssc-ae-all-a06&ad_id=376006095084&gclid=CjwKCAiAx_DwBRAfEiwA3vwZYoF32aT08S9010-igRja12gtz8SpTPer5CgGagvVu55v4yPeAj7gDRoCuZoQAvD_BwE&cur_warehouse=CN][Banggood Link]]   |
|---------------------------------+--------+-----------------|
| Total                           | 763.62 |                 |
|---------------------------------+--------+-----------------|
#+TBLFM: @>$2=vsum(@II..@III)

* Make an appointment with Mr Musleh for PCB etching
